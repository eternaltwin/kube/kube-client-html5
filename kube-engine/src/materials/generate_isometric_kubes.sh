shopt -s extglob

mkdir -p iso
for f in $(find . -regextype egrep -maxdepth 1 -type f -regex ".*[0-9]{2}\.png")
do
    up=$f
    wo_ext=$(echo "$f" | rev | cut -f 2- -d '.' | rev)
    upf=${wo_ext}_top.png
    downf=${wo_ext}_top_bottom.png
    if [ -f $upf ]; then
        up=$upf
    fi
    if [ -f $downf ]; then
        up=$downf
    fi 
    convert \
         \( $up -scale 512 -alpha set -virtual-pixel transparent \
            +distort Affine '0,512 0,0   0,0 -87,-50  512,512 86,-50' \) \
         \( $f -scale 512 -alpha set -virtual-pixel transparent \
            +distort Affine '512,0 0,0   0,0 -87,-50  512,512 0,100' \) \
         \( $f -scale 512 -alpha set -virtual-pixel transparent \
            +distort Affine '  0,0 0,0   0,512 0,100    512,0 86,-50' \) \
         -background none -compose plus -layers merge +repage \
         -compose over -scale 36 iso/$f
done
