export interface KubeDataFlashVars {
    _flags: number
    _force: boolean
    _imax: number
    _inv: (number | null)[]
    _pow: number
    _replay: unknown // Old thing for demo mode
    _s: string
    _swim: number | null
    _tuto: number
    _u: number
    _uid: number
    _x: number
    _y: number
    _z: number
}

export interface KubeFlashVars {
    data: KubeDataFlashVars
    k: string
    sid: string
    texts: { [key: string]: string }
}

export interface MapDataFlashVars {
    _dol: unknown
    _s: string
    _x: number
    _y: number
}

export interface MapFlashVars {
    infos: MapDataFlashVars
    k: string
    sid: string
}
