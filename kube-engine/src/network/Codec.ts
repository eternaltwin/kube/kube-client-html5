// import { Buffer } from "buffer"
//
// const KUBE_KEY_PREFIX: string = "77e352"
// const BASE64_CHARS: string = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_"
//
// export class Codec {
//     permutations: Uint8Array // 256
//
//     key: string
//     keyPrefix: string
//
//     get fullKey(): string {
//         return this.keyPrefix + this.key
//     }
//
//     constructor(key: string, keyPrefix: string = KUBE_KEY_PREFIX) {
//         this.keyPrefix = keyPrefix
//         this.key = key
//
//         this.computePermutation()
//     }
//
//     computePermutation() {
//         let perm = new Uint8Array(256)
//         for (let i = 0; i < 256; i++) perm[i] = i & 0x7f
//
//         let j = 0
//         for (let i = 0; i < 256; i++) {
//             j += perm[i] + this.fullKey.charCodeAt(i % this.fullKey.length)
//             j &= 0x7f
//             ;[perm[i], perm[j]] = [perm[j], perm[i]]
//         }
//         this.permutations = perm
//     }
//
//     encodeCrc(crc: number) {
//         return String.fromCharCode(
//             BASE64_CHARS.charCodeAt(crc & 63),
//             BASE64_CHARS.charCodeAt((crc >> 6) & 63),
//             BASE64_CHARS.charCodeAt((crc >> 12) & 63),
//             BASE64_CHARS.charCodeAt((crc >> 18) & 63)
//         )
//     }
//
//     encode(buffer: string) {
//         let crc1 = this.permutations[0]
//         let crc2 = this.permutations[1]
//
//         let stringBuilder = ""
//
//         for (let i = 0; i < buffer.length; i++) {
//             const b = buffer.charCodeAt(i)
//             let test = b ^ this.permutations[i & 255]
//             stringBuilder += String.fromCharCode(test == 0 ? b : test)
//             crc1 = (crc1 + test) % 65521
//             crc2 = (crc2 + crc1) % 65521
//         }
//         let crc = crc1 ^ (crc2 << 8)
//
//         return stringBuilder + this.encodeCrc(crc)
//     }
//
//     decode(buffer: string) {
//         let crcCheck = buffer.slice(-4)
//         buffer = buffer.slice(0, -4)
//
//         let crc1 = this.permutations[0]
//         let crc2 = this.permutations[1]
//
//         let stringBuilder = ""
//
//         for (let i = 0; i < buffer.length; i++) {
//             const b = buffer.charCodeAt(i)
//             let test = b ^ this.permutations[i & 255]
//             let crcSave = test == 0 ? 0 : b
//             stringBuilder += String.fromCharCode(test == 0 ? b : test)
//
//             crc1 = (crc1 + crcSave) % 65521
//             crc2 = (crc2 + crc1) % 65521
//         }
//         let crc = crc1 ^ (crc2 << 8)
//
//         if (this.encodeCrc(crc) != crcCheck) {
//             console.error(this.encodeCrc(crc), "!=", crcCheck)
//             console.error(buffer, "=>", stringBuilder)
//             console.error(new Buffer(buffer), "=>", new Buffer(stringBuilder))
//             throw new Error("Invalid CRC")
//         }
//
//         return stringBuilder
//     }
// }
