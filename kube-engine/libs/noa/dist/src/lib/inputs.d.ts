/**
 * @internal
 * @returns {GameInputs}
 */
export function createInputs(noa: any, opts: any, element: any): GameInputs;
import { GameInputs } from "game-inputs";
