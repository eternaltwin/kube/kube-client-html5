Some libs included to edit them easily.

- noa-engine: Engine that use babylon.js
  - TODO
- game-inputs: Used by noa-engine to handle inputs
  - Edit mouse events to allow moving camera outside the game div (bind to window).
- voxel-physics-engine: Used by noa-engine to handle physics

# TODO

We should rewrite some parts (or everything) of the engine. We do not need an ECS, complex physics, and lot of code.