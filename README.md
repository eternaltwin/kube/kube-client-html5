# Kube client implementation

## TODO
- Adjust physics settings to match Kube physics

## RE

### Protocol

- A socket is opened for each loaded chunk
    - This allows receiving chunk updates (after a `CWatch`)
    - There is no "CUnwatch" command because the socket is closed when the chunk is unloaded.
 - Other commands like `CSetBlocks` are sent with a random opened socket
 - `CSavePhoto` is a exception : a new socket is opened for this command. (why ? the socket is never closed ?)
 
 ### When to send commands
 - `CSavePos` sent when the last save is at least 15 blocks away