// ==UserScript==
// @name         Kube HTML5
// @namespace    Th0m45Lup1n-Kub3-w3b5cr1pt
// @version      0.1.4
// @author       @TheToto (Thomas Lupin), Edited by @Angelisium (알렉시스)
// @description  Replaces the Flash client with an HTML5 client.
// @icon         https://www.google.com/s2/favicons?sz=64&domain=kube.muxxu.com
// @updateURL    https://gitlab.com/eternaltwin/kube/kube-client-html5/-/raw/master/userscript/kube_replace_flash.user.js
// @downloadURL  https://gitlab.com/eternaltwin/kube/kube-client-html5/-/raw/master/userscript/kube_replace_flash.user.js
// @match        http*://kube.muxxu.com/*
// @match        http*://kube.*.muxxu.com/*
// @grant        none
// ==/UserScript==

;(function () {
    // Feel free to use your own websocket bridge !
    // https://github.com/novnc/websockify
    // $ websockify PORT play.kube.muxxu.com:6767
    const WEBSOCKET_BRIDGE = "wss://kube-ws-bridge.thetoto.fr"
    const IFRAME_URL = "https://eternaltwin.gitlab.io/kube/kube-client-html5"
    //const IFRAME_URL = "http://localhost:1234";

    const flashvars = document.body.innerHTML.match(/so.addParam\("FlashVars","([^"]+)\"\);/)[1]

    if (window.location.pathname === "/") {
        const iframe = document.createElement("iframe")
        iframe.allow = "fullscreen"
        iframe.src = IFRAME_URL + "/#/game/flashvars"
        iframe.style = "border: none !important;"
        iframe.height = 320
        iframe.width = 812

        const game = document.querySelector("#mxcontent > .game")
        if (game) {
            game.replaceChildren(iframe)
        }

        window.addEventListener("message", function (e) {
            if (e?.data === "ready") {
                console.log("Sending flashVars to iframe")
                iframe.contentWindow.postMessage(
                    {
                        flashVars: flashvars,
                        websocketBridge: WEBSOCKET_BRIDGE,
                    },
                    "*"
                )
            } else if (e?.data?.startsWith("redirect:")) {
                let url = e.data.slice("redirect:".length)
                console.log("redirect to", url)
                window.location.replace(url)
            }
        })
    }
    if (window.location.pathname === "/map") {
        const iframe = document.createElement("iframe")
        iframe.allow = "fullscreen"
        iframe.src = IFRAME_URL + "/#/game/map/flashvars"
        iframe.style = "border: none !important;"
        iframe.height = 812
        iframe.width = 812

        const game = document.querySelector("#mxcontent > #swf_map")
        if (game) {
            game.replaceChildren(iframe)
        }

        window.addEventListener("message", function (e) {
            if (e?.data === "ready") {
                console.log("Sending flashVars to iframe")
                iframe.contentWindow.postMessage(
                    {
                        flashVars: flashvars,
                        websocketBridge: WEBSOCKET_BRIDGE,
                    },
                    "*"
                )
            } else if (e?.data?.startsWith("redirect:")) {
                let url = e.data.slice("redirect:".length)
                console.log("redirect to", url)
                window.location.replace(url)
            }
        })
    }
})()
