// ==UserScript==
// @name         Kube Tools
// @namespace    4ng3l1s1um-Kub3-w3b5cr1pt
// @version      0.0.5
// @author       @Angelisium (알렉시스)
// @description  Add some tools
// @icon         https://www.google.com/s2/favicons?sz=64&domain=kube.muxxu.com
// @updateURL    https://gitlab.com/eternaltwin/kube/kube-client-html5/-/raw/master/userscript/kube_tools.user.js
// @downloadURL  https://gitlab.com/eternaltwin/kube/kube-client-html5/-/raw/master/userscript/kube_tools.user.js
// @match        https://eternaltwin.gitlab.io/kube/kube-client-html5/*
// @match        http://localhost:1234/*
// @match        http*://kube.muxxu.com/*
// @match        http*://kube.*.muxxu.com/*
// @grant        unsafeWindow
// ==/UserScript==

const CONFIG = {
    show_popup: false,
    fly: true
};

const popup_content = {
    emv_error: "Vous ne pouvez pas acheter cet objet car il vous reste encore suffisamment d'énergie et il vous serait donc inutile.",
    emv_success: "Boire cette délicieuse Eau Vital vous a revitalisé ! Vous devriez pouvoir poursuivre votre aventure sportive pendant encore quelque temps."
};

var isKube = (window.location.host === "kube.muxxu.com");
var html_parser = new DOMParser();
var temp = {};

function pause(time) {
	return new Promise((resolve, reject) => setTimeout(resolve, time));
}

function send(action, arguments) {
    let target = isKube ? document.querySelector('iframe').contentWindow : window.parent,
        message = arguments;
    message.action = action;
    message.is = "Angelisium-tools";
    target.postMessage(message, "*");
}

function findPopupID(text) {
    for(let id in popup_content) {
        if(popup_content[id] === text) {
            return id;
        }
    }
    throw "Unknow popup : " + text;
}

function onFlyInput(event) {
    if(event.key === "a") {
        unsafeWindow.game.noa.physics.bodies[0].velocity[1] = 10
    }
    if(event.key === "e") {
        unsafeWindow.game.noa.physics.bodies[0].velocity[1] = -10
    }
}

var saveGravity;

function startFly() {
    temp.fly = true;
    saveGravity = unsafeWindow.game.noa.physics.gravity;
    unsafeWindow.game.noa.physics.gravity = [0, 0, 0];
    unsafeWindow.game.noa.physics.bodies[0].airDrag = 3;
    document.addEventListener("keydown", onFlyInput);
    console.log("started fly");
}

function stopFly() {
    temp.fly = false;
    unsafeWindow.game.noa.physics.gravity = saveGravity;
    unsafeWindow.game.noa.physics.bodies[0].airDrag = -1;
    document.removeEventListener("keydown", onFlyInput);
    console.log("stoped fly");
}

async function onMessage(event) {
    console.log(event);
    if(event?.data?.is === "Angelisium-tools") {
        let d = event.data;
        if(isKube) {
            if(d.action === "nopower") {
                let buy = await fetch("http://kube.muxxu.com/shop?buy=water;chk=" + d.key, {
                        "method": "GET",
                        "referrer": "http://kube.muxxu.com/shop",
                        "redirect": "manual"
                    }),
                    popup = await fetch("http://kube.muxxu.com/shop").then(r => r.text());
                popup = html_parser.parseFromString(popup, 'text/html');
                popup = popup.querySelector('#notification');
                if(CONFIG.show_popup === true) {
                    let mxhead = document.querySelector('.mxhead');
                    mxhead.insertBefore(popup, mxhead.firstChild);
                }
                let popup_id = findPopupID(popup.querySelector('.ptext').textContent);
                if(popup_id === "emv_success") {
                    send("fill_power", {});
                } else {
                    window.location.reload();
                }
            }
        } else {
            if(d.action === "teleport") {
                unsafeWindow.game.teleport(d.x, d.z, d.y);
            } else if(d.action === "fill_power") {
                unsafeWindow.game.player.power = 10000;
                unsafeWindow.game.emit("uiPower", 10000);
            } else if(d.action === "fly") {
                if(d.mode === "auto") {
                    if(!temp.fly || temp.fly === false) {
                        startFly();
                    } else {
                        stopFly();
                    }
                } else {
                    if(d.mode === "on") {
                        startFly();
                    } else if(d.mode === "off") {
                        stopFly();
                    }
                }
            }
        }
    }
}

function initialization() {
    if(isKube) {
        unsafeWindow.teleport = function(x, y, z = 50) {
            let rx = ((x + 1) * 32) - 16,
                ry = ((y + 1) * 32) - 16;
            return send("teleport", {
                x: rx,
                y: ry,
                z: z
            });
        };
        unsafeWindow.fly = function(m) {
            return send("fly", {
                mode: (m && ["off", "on"].indexOf(m) > -1) ? m : "auto"
            });
        }
        document.querySelector('#tid_bar_down').remove();
    } else {
        unsafeWindow.game.on("uiPower", function(power) {
            if(power === 0) {
                send("nopower", {
                    key: unsafeWindow.game.client.codec.key
                });
            }
        });
    }
    window.addEventListener("message", onMessage, false);
}

(async function() {
    "use strict";
    if(!isKube) {
        while(!unsafeWindow.game) {
            await pause(800);
        }
    }
    initialization();
})();
